<?php
/**
 * @file
 * commerce_stock_transaction_feature.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_stock_transaction_feature_default_rules_configuration() {
  $items = array();
  $items['rules_build_a_list_of_line_item_ids'] = entity_import('rules_config', '{ "rules_build_a_list_of_line_item_ids" : {
      "LABEL" : "Build a list of line_item ids",
      "PLUGIN" : "action set",
      "TAGS" : [ "line item", "stock" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "list_commerce_line_items" : {
          "label" : "list-commerce-line-items",
          "type" : "list\\u003ccommerce_line_item\\u003e"
        },
        "list_line_item_ids" : {
          "label" : "list-line-item-ids",
          "type" : "list\\u003cinteger\\u003e",
          "parameter" : false
        }
      },
      "ACTION SET" : [
        { "LOOP" : {
            "USING" : { "list" : [ "list-commerce-line-items" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [
              { "list_add" : {
                  "list" : [ "list-line-item-ids" ],
                  "item" : [ "list-item:line-item-id" ]
                }
              }
            ]
          }
        }
      ],
      "PROVIDES VARIABLES" : [ "list_line_item_ids" ]
    }
  }');
  $items['rules_create_a_stock_entry'] = entity_import('rules_config', '{ "rules_create_a_stock_entry" : {
      "LABEL" : "Create a stock entry (no reference)",
      "PLUGIN" : "rule",
      "TAGS" : [ "stock" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "commerce_product" : { "label" : "commerce-product", "type" : "commerce_product" },
        "stock_entry_quantity" : { "label" : "stock-entry-quantity", "type" : "integer" }
      },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-product" ], "field" : "commerce_stock" } }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "stock_entry",
              "param_title" : "Stock entry for product #sku [commerce-product:sku]. (by [site:current-user] - [site:current-date])",
              "param_author" : [ "site:current-user" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-stock-entry-product" ],
            "value" : [ "commerce-product" ]
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-stock-entry-quantity" ],
            "value" : [ "stock-entry-quantity" ]
          }
        },
        { "entity_save" : { "data" : [ "entity-created" ] } }
      ]
    }
  }');
  $items['rules_create_a_stock_entry_with_line_item_ref'] = entity_import('rules_config', '{ "rules_create_a_stock_entry_with_line_item_ref" : {
      "LABEL" : "Create a stock entry (with line item reference)",
      "PLUGIN" : "rule",
      "TAGS" : [ "stock" ],
      "REQUIRES" : [ "rules", "commerce_stock_transaction_feature" ],
      "USES VARIABLES" : { "line_item" : { "label" : "line-item", "type" : "commerce_line_item" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "commerce_product" } },
        { "entity_has_field" : {
            "entity" : [ "line-item:commerce-product" ],
            "field" : "commerce_stock"
          }
        }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "stock_entry",
              "param_title" : "Stock entry for product #sku [line-item:commerce-product:sku] - Order #[line-item:order-id]. (by [site:current-user] - [site:current-date])",
              "param_author" : [ "site:current-user" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-stock-entry-product" ],
            "value" : [ "line-item:commerce-product" ]
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-stock-entry-line-item-id" ],
            "value" : [ "line-item:line-item-id" ]
          }
        },
        { "commerce_stock_transaction_feature_data_convert_action" : {
            "USING" : {
              "from_type" : "decimal",
              "input" : [ "line-item:quantity" ],
              "to_type" : "integer"
            },
            "PROVIDE" : { "conversion_result" : { "conversion_result" : "Conversion result" } }
          }
        },
        { "data_calc" : {
            "USING" : { "input_1" : [ "conversion-result" ], "op" : "*", "input_2" : "-1" },
            "PROVIDE" : { "result" : { "result" : "Calculation result" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-stock-entry-quantity" ],
            "value" : [ "result" ]
          }
        },
        { "entity_save" : { "data" : [ "entity-created" ] } }
      ]
    }
  }');
  $items['rules_delete_stock_entries_by_order_'] = entity_import('rules_config', '{ "rules_delete_stock_entries_by_order_" : {
      "LABEL" : "Delete stock entries (by order)",
      "PLUGIN" : "action set",
      "TAGS" : [ "order", "stock" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "commerce_order" : { "label" : "commerce-order", "type" : "commerce_order" } },
      "ACTION SET" : [
        { "component_rules_build_a_list_of_line_item_ids" : {
            "USING" : { "list_commerce_line_items" : [ "commerce-order:commerce-line-items" ] },
            "PROVIDE" : { "list_line_item_ids" : { "list_line_item_ids" : "list-line-item-ids" } }
          }
        },
        { "entity_query" : {
            "USING" : {
              "type" : "node",
              "property" : "field_stock_entry_line_item_id",
              "value" : [ "list-line-item-ids" ],
              "limit" : "100"
            },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "entity-fetched" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [ { "entity_delete" : { "data" : [ "list-item" ] } } ]
          }
        }
      ]
    }
  }');
  $items['rules_get_current_commerce_stock_value'] = entity_import('rules_config', '{ "rules_get_current_commerce_stock_value" : {
      "LABEL" : "Get current commerce stock value (wrapper)",
      "PLUGIN" : "rule set",
      "TAGS" : [ "product", "stock" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "commerce_product" : { "label" : "commerce-product", "type" : "commerce_product" },
        "current_product_stock_value" : {
          "label" : "current-product-stock-value",
          "type" : "integer",
          "parameter" : false
        }
      },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "commerce-product" ], "field" : "commerce_stock" } },
              { "data_is_empty" : { "data" : [ "commerce-product:commerce-stock" ] } }
            ],
            "DO" : [
              { "data_set" : { "data" : [ "current-product-stock-value" ], "value" : "0" } }
            ],
            "LABEL" : "Get commerce stock value (If stock field is empty)"
          }
        },
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "commerce-product" ], "field" : "commerce_stock" } },
              { "NOT data_is_empty" : { "data" : [ "commerce-product:commerce-stock" ] } }
            ],
            "DO" : [
              { "data_set" : {
                  "data" : [ "current-product-stock-value" ],
                  "value" : [ "commerce-product:commerce-stock" ]
                }
              }
            ],
            "LABEL" : "Get commerce stock value (If stock field has value)"
          }
        }
      ],
      "PROVIDES VARIABLES" : [ "current_product_stock_value" ]
    }
  }');
  $items['rules_synchronize_stock_entries_line_item_create_'] = entity_import('rules_config', '{ "rules_synchronize_stock_entries_line_item_create_" : {
      "LABEL" : "Synchronize stock entries (Line item create)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "stock" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_line_item_insert" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "OR" : [
            { "data_is" : { "data" : [ "commerce-line-item:order:state" ], "value" : "completed" } },
            { "data_is" : { "data" : [ "commerce-line-item:order:state" ], "value" : "pending" } }
          ]
        },
        { "NOT data_is_empty" : { "data" : [ "commerce-line-item:order-id" ] } }
      ],
      "DO" : [
        { "component_rules_create_a_stock_entry_with_line_item_ref" : { "line_item" : [ "commerce-line-item" ] } }
      ]
    }
  }');
  $items['rules_synchronize_stock_entries_line_item_deleted'] = entity_import('rules_config', '{ "rules_synchronize_stock_entries_line_item_deleted" : {
      "LABEL" : "Synchronize stock entries (Line item deleted)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "stock" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_line_item_delete" ],
      "DO" : [
        { "entity_query" : {
            "USING" : {
              "type" : "node",
              "property" : "field_stock_entry_line_item_id",
              "value" : [ "commerce-line-item:line-item-id" ]
            },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "entity-fetched" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [ { "entity_delete" : { "data" : [ "list-item" ] } } ]
          }
        }
      ]
    }
  }');
  $items['rules_synchronize_stock_entries_line_item_update'] = entity_import('rules_config', '{ "rules_synchronize_stock_entries_line_item_update" : {
      "LABEL" : "Synchronize stock entries (Line item update)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "stock" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_line_item_update" ],
      "IF" : [
        { "NOT data_is_empty" : { "data" : [ "commerce-line-item:order-id" ] } },
        { "OR" : [
            { "data_is" : { "data" : [ "commerce-line-item:order:state" ], "value" : "pending" } },
            { "data_is" : { "data" : [ "commerce-line-item:order:state" ], "value" : "completed" } }
          ]
        }
      ],
      "DO" : [
        { "entity_query" : {
            "USING" : {
              "type" : "node",
              "property" : "field_stock_entry_line_item_id",
              "value" : [ "commerce-line-item-unchanged:line-item-id" ]
            },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "entity-fetched" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [ { "entity_delete" : { "data" : [ "list-item" ] } } ]
          }
        },
        { "component_rules_create_a_stock_entry_with_line_item_ref" : { "line_item" : [ "commerce-line-item" ] } }
      ]
    }
  }');
  $items['rules_synchronize_stock_entries_order_update'] = entity_import('rules_config', '{ "rules_synchronize_stock_entries_order_update" : {
      "LABEL" : "Synchronize stock entries (Order update) \\u003e= Pending",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "stock" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_order_update" ],
      "IF" : [
        { "OR" : [
            { "data_is" : { "data" : [ "commerce-order:state" ], "value" : "pending" } },
            { "data_is" : { "data" : [ "commerce-order:state" ], "value" : "completed" } }
          ]
        },
        { "OR" : [
            { "data_is" : { "data" : [ "commerce-order-unchanged:state" ], "value" : "canceled" } },
            { "data_is" : { "data" : [ "commerce-order-unchanged:state" ], "value" : "cart" } },
            { "data_is" : { "data" : [ "commerce-order-unchanged:state" ], "value" : "checkout" } }
          ]
        }
      ],
      "DO" : [
        { "component_rules_delete_stock_entries_by_order_" : { "commerce_order" : [ "commerce-order-unchanged" ] } },
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "order_line_item" : "Order line item" },
            "DO" : [
              { "component_rules_create_a_stock_entry_with_line_item_ref" : { "line_item" : [ "order-line-item" ] } }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_synchronize_stock_entries_order_update_pending'] = entity_import('rules_config', '{ "rules_synchronize_stock_entries_order_update_pending" : {
      "LABEL" : "Synchronize stock entries (Order update) \\u003c Pending",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "stock" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_order_update" ],
      "IF" : [
        { "OR" : [
            { "data_is" : { "data" : [ "commerce-order:state" ], "value" : "canceled" } },
            { "data_is" : { "data" : [ "commerce-order:state" ], "value" : "cart" } },
            { "data_is" : { "data" : [ "commerce-order:state" ], "value" : "checkout" } }
          ]
        },
        { "OR" : [
            { "data_is" : { "data" : [ "commerce-order-unchanged:state" ], "value" : "pending" } },
            { "data_is" : { "data" : [ "commerce-order-unchanged:state" ], "value" : "completed" } }
          ]
        }
      ],
      "DO" : [
        { "component_rules_delete_stock_entries_by_order_" : { "commerce_order" : [ "commerce-order-unchanged" ] } }
      ]
    }
  }');
  $items['rules_update_product_stock_value'] = entity_import('rules_config', '{ "rules_update_product_stock_value" : {
      "LABEL" : "Update product stock value (Stock Entry created)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "product", "stock" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_stock_entry_quantity" } },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_stock_entry_product" } },
        { "entity_has_field" : {
            "entity" : [ "node:field-stock-entry-product" ],
            "field" : "commerce_stock"
          }
        }
      ],
      "DO" : [
        { "component_rules_get_current_commerce_stock_value" : {
            "USING" : { "commerce_product" : [ "node:field-stock-entry-product" ] },
            "PROVIDE" : { "current_product_stock_value" : { "current_product_stock_value" : "current-product-stock-value" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "node:field-stock-entry-quantity" ],
              "op" : "+",
              "input_2" : [ "current-product-stock-value" ]
            },
            "PROVIDE" : { "result" : { "new_commerce_stock_value" : "Calculation result" } }
          }
        },
        { "data_set" : {
            "data" : [ "node:field-stock-entry-product:commerce-stock" ],
            "value" : [ "new-commerce-stock-value" ]
          }
        }
      ]
    }
  }');
  $items['rules_update_product_stock_value_stock_entry_deleted'] = entity_import('rules_config', '{ "rules_update_product_stock_value_stock_entry_deleted" : {
      "LABEL" : "Update product stock value (Stock Entry deleted)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "product", "stock" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_delete" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_stock_entry_quantity" } },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_stock_entry_product" } },
        { "entity_has_field" : {
            "entity" : [ "node:field-stock-entry-product" ],
            "field" : "commerce_stock"
          }
        }
      ],
      "DO" : [
        { "component_rules_get_current_commerce_stock_value" : {
            "USING" : { "commerce_product" : [ "node:field-stock-entry-product" ] },
            "PROVIDE" : { "current_product_stock_value" : { "current_product_stock_value" : "current-product-stock-value" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "current-product-stock-value" ],
              "op" : "-",
              "input_2" : [ "node:field-stock-entry-quantity" ]
            },
            "PROVIDE" : { "result" : { "new_commerce_stock_value" : "Calculation result" } }
          }
        },
        { "data_set" : {
            "data" : [ "node:field-stock-entry-product:commerce-stock" ],
            "value" : [ "new-commerce-stock-value" ]
          }
        }
      ]
    }
  }');
  $items['rules_update_product_stock_value_updated'] = entity_import('rules_config', '{ "rules_update_product_stock_value_updated" : {
      "LABEL" : "Update product stock value (Stock Entry updated)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "stock" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_stock_entry_quantity" } },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_stock_entry_product" } },
        { "entity_has_field" : {
            "entity" : [ "node:field-stock-entry-product" ],
            "field" : "commerce_stock"
          }
        },
        { "data_is" : {
            "data" : [ "node:field-stock-entry-product" ],
            "value" : [ "node-unchanged:field-stock-entry-product" ]
          }
        }
      ],
      "DO" : [
        { "component_rules_get_current_commerce_stock_value" : {
            "USING" : { "commerce_product" : [ "node-unchanged:field-stock-entry-product" ] },
            "PROVIDE" : { "current_product_stock_value" : { "current_product_stock_value" : "current-product-stock-value" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "node-unchanged:field-stock-entry-quantity" ],
              "op" : "-",
              "input_2" : [ "node:field-stock-entry-quantity" ]
            },
            "PROVIDE" : { "result" : { "stock_delta" : "stock-delta" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "current-product-stock-value" ],
              "op" : "-",
              "input_2" : [ "stock-delta" ]
            },
            "PROVIDE" : { "result" : { "new_stock_result" : "new-stock-result" } }
          }
        },
        { "data_set" : {
            "data" : [ "node:field-stock-entry-product:commerce-stock" ],
            "value" : [ "new-stock-result" ]
          }
        }
      ]
    }
  }');
  return $items;
}
