<?php
/**
 * @file
 * commerce_stock_transaction_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function commerce_stock_transaction_feature_node_info() {
  $items = array(
    'stock_entry' => array(
      'name' => t('Stock Entry'),
      'base' => 'node_content',
      'description' => t('A stock transaction entry'),
      'has_title' => '1',
      'title_label' => t('Short description'),
      'help' => t('Create a stock entry that will automatically update your product stock value.'),
    ),
  );
  return $items;
}
